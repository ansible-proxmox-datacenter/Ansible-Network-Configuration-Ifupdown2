Role Name
=========

This role writes network configuration to /etc/interfaces. 
This is not the cleanest way to write configuration, but Proxmox (aim of my roles) doesn't support interfaces.d

Requirements
------------

No specific requirements. just be sure to use ifupdown and not systemd/networkd. 

Role Variables
--------------

The main variable networksList is a list of configs to write. 
Example
```
networksList:
  local: 
    interface: lo
    type: loopback
    mode: loopback
  network1: 
    interface: enp0s1
    mode: [manual, static]
    type: [interface, bridge, loopback]
    bridge_port: interface to bridge # Add mac_address var
    macAddress: xx:xx:xx:xx:xx:xx # Set mac address for bridge
    ip: x.x.x.x
    mask: 24
    gateway: x.x.x.x
    dns: 
    - x.x.x.x
    - x.x.x.x
    comment: comments to add in file.
  network2: 
    interface: enp0s2
    mode: [manual, static]
    type: [interface, bridge, loopback]
    bridge_port: interface to bridge # Add mac_address var
    macAddress: xx:xx:xx:xx:xx:xx # Set mac address for bridge
    ip: x.x.x.x
    mask: 24
    gateway: x.x.x.x
    dns: 
    - x.x.x.x
    - x.x.x.x
    comment: comments to add in file.
```

Dependencies
------------

No other role dependencie. 

Example Playbook
----------------
```
- hosts: proxmoxve
  roles: 
    - role: Ansible-Network-Configuration-Ifupdown2
      networksList: "[network1, network2]"
```
License
-------

GNU GPLv3

Author Information
------------------

Passionate Sysadmin : https://yanux.info
